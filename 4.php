<?php
$precio = 25;
$iva10 = 0.1;
$precioIva = 0;
$precioFinal = 0;

//procesamiento
$precioIva = $precio * $iva10;
$precioFinal = $precioIva + $precio;

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 4</title>
</head>

<body>
    <ul>
        <li>El precio del producto es <?= $precio ?>€</li>
        <li>El iva correspondiente al 10% es <?= $precioIva ?>€</li>
        <li>El precio final del producto es <?= $precioFinal ?>€</li>
    </ul>
</body>

</html>