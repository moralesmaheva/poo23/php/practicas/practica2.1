<?php
// calcular el area de un triangulo
$base = 4;
$altura = 6;
$area = 0;

// procesamiento
$area = ($base * $altura) / 2;

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 1</title>
</head>

<body>

    <div>
        El área del triángulo es <?= $area ?>
    </div>

</body>

</html>